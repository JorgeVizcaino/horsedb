// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var mysql_conn = require("./mysql_local").connection();
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var sessions = require("client-sessions"); 

//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);
app.use(allowCrossDomain);
	app.use(sessions({ cookieName: 'session', //cookie name dictates the key name added to the request object 
	secret: 'blargadeeblargblarg', // should be a large unguessable string 
	duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms 
	activeDuration: 1000 * 60 * 5 // if expiresIn 
}));

// var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();      

router.route('/logIn').post(function(req,res){
	res.json("hola mundo");
});

router.route('/getUsers').get(function(req, res){
	console.log("get Users GET");
	var query = "select * from users where date = '' and region = ''";
	connection.query(query, function(err, results) {
	  if (err) throw err;
	  	  
		res.json(results);
	});

});

router.route('/getMessage').get(function(req, res){
	console.log("GET getMessage");
	res.json("GET getMessage");
	var query = "select * from users where user = '' and read = 0";

});

router.route('/registerUser').post(function(req, res){
	console.log("POST registerUser");
	res.json("POST registerUser");
	var query = "insert into users(username, firstname, lastname, email, date_created)";

});

router.route('/sendMessage').post(function(req, res){
	console.log("POST sendMessage");
	var query = "insert into messages(message, user_from, user_to, date_sent, read) " + 
	"values('" + req.body.message + "', '" + req.body.user_from + "', '" + req.body.user_to + 
		"', NOW(), 0)";
	res.json(query);

	mysql_conn.connect();
	mysql_conn.query(query, function(err, rows, fields) {});
	mysql_conn.end();

});

app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(8080);
console.log('Starting server on port 8080');