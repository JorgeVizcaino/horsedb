
$(document).ready(function(){
        $("#registerUser").click(function(){
                var postdata = {};
                postdata["userId"] = $("#userId").val();
                postdata["firstName"] = $("#firstName").val();
                postdata["lastName"] = $("#lastName").val();
                postdata["ocupation"] = $("#ocupation").val();
                postdata["phone"] = $("#phone").val();
                postdata["password"] = $("#password").val();
                postdata["age"] = $("#age").val();
                postdata["email"] = $("#email").val();
                postdata["username"] = $("#username").val();
                $.post("http://54.201.119.100:8080/registerUser", postdata, function(data){
                        console.log(data);
                }, "json");

        });

        $("#registerHorse").click(function(){
                var postdata = {};
                postdata["horseId"] = $("#horseId").val();
                postdata["horseName"] = $("#ironworkHorse_FK").val();
                postdata["userHorse_FK"] = $("#userHorse_FK").val();
                postdata["feedingHorse_FK"] = $("#feedingHorse_FK").val();
                postdata["ironworkHorse_FK"] = $("#ironworkHorse_FK").val();
                postdata["horseType"] = $("#horseType").val();
                postdata["horseBreed"] = $("#horseBreed").val();
                postdata["horseAge"] = $("#horseAge").val();
                $.post("http://54.201.119.100:8080/registerHorse", postdata, function(data){
                        console.log(data);
                }, "json");

        });

        $("#addDiet").click(function(){
                var postdata = {};
                postdata["feedId"] = $("#feedId").val();
                postdata["breakfast"] = $("#breakfast").val();
                postdata["suplementBreakfast"] = $("#suplementBreakfast").val();
                postdata["timeBreakfast"] = $("#timeBreakfast").val();
                postdata["lunch"] = $("#lunch").val();
                postdata["suplementLunch"] = $("#suplementLunch").val();
                postdata["timeLunch"] = $("#timeLunch").val();
                postdata["dinner"] = $("#dinner").val();
                postdata["suplementDinner"] = $("#suplementDinner").val();
                postdata["timeDinner"] = $("#timeDinner").val();
                postdata["vitamins"] = $("#vitamins").val();
                postdata["feedingNotes"] = $("#feedingNotes").val();
                $.post("http://54.201.119.100:8080/addDiet", postdata, function(data){
                        console.log(data);
                }, "json");

        });

        $("#addIronWork").click(function(){
                var postdata = {};
                postdata["ironWorkId"] = $("#ironWorkId").val();
                postdata["lastIrWork"] = $("#lastIrWork").val();
                postdata["nextIrWork"] = $("#nextIrWork").val();
                postdata["blacksmithName"] = $("#blacksmithName").val();
                postdata["anotations"] = $("#anotations").val();
                $.post("http://54.201.119.100:8080/addIronWork", postdata, function(data){
                        console.log(data);
                }, "json");

        });

        $("#medicalConsult").click(function(){
                var postdata = {};
                postdata["mhId"] = $("#mhId").val();
                postdata["horsePatient_FK"] = $("#horsePatient_FK").val();
                postdata["vetHorse_FK"] = $("#vetHorse_FK").val();
                postdata["disease"] = $("#disease").val();
                postdata["medicines"] = $("#medicines").val();
                postdata["anotations"] = $("#anotations").val();
                postdata["dateConsult"] = $("#dateConsult").val();
                $.post("http://54.201.119.100:8080/medicalConsult", postdata, function(data){
                        console.log(data);
                }, "json");

        });


        $("#deleteUsers").click(function(){

                /*$.post("http://54.201.119.100:8080/deleteUsers", function(data){
                        console.log(data);
                }, "json");*/

                $.ajax({
                url: 'http://54.201.119.100:8080/deleteUsers',
                type: 'DELETE',
                success: function(result) {
                        console.log(result);
                        alert('The users have been deleted');
                },

                error: function(result) {
                        console.log(result);
                        alert('Something went wrong');
                }
                });



        });

        $("#logInB").click(function(){


                var postdata = {};
                postdata["username"] = $("#userNamelogIn").val();
                postdata["password"] = $("#passwordlogIn").val();

                $.post("http://54.201.119.100:8080/logIn", postdata, function(data){
                        console.log(data);
                }, "json");

                
        });

        $("#logOutB").click(function(){


                $.post("http://54.201.119.100:8080/logOut", function(data){
                        console.log(data);
                }, "json");

                
        });

        $("#deleteRowUsers").click(function(){

                var postdata = {};
                postdata["username"] = $("#delUsername").val();
                /*
                $.post("http://54.201.119.100:8080/deleteRowUsers", postdata, function(data){
                        console.log(data);
                }, "json");*/

                $.ajax({
                url: 'http://54.201.119.100:8080/deleteRowUsers',
                type: 'DELETE',
                data: postdata,
                success: function(result) {
                        console.log(result);
                        alert('One user has been deleted');
                },

                error: function(result) {
                        console.log(result);
                        alert('Something went wrong');
                }
                });

        });

});

