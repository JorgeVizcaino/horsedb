var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	first_name: {type: String, require:true},
	last_name: {type: String, require:true},
	email: {type: String, require:true},
	password: {type: String, require:true},
	ocupation: {type: String, require:true}
});
module.exports = mongoose.model('user', UserSchema);