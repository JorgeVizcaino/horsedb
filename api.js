// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var db = require("./mysql_local");
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
//var sessions = require("client-sessions");


//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);


//Cookie
//===================================================================================
/*app.use(sessions({
  cookieName: 'session', // cookie name dictates the key name added to the request object 
  secret: 'blargadeeblargblarg', // should be a large unguessable string 
  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms 
  activeDuration: 1000 * 60 * 5 // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds 
}));*/




var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();               


router.route('/logIn').post(function(req, res){

  var mysql_conn = db.connection();
  var query = "select * from User where password = MD5('"+req.body.password+"') and username = '"+req.body.username+"';"

  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log(rows.length);
    
    if(rows.length == 1){
      req.session.user = rows[0];
      res.json({ message: "SUCCESS. Log In: "  + rows[0]});
      console.log('Log IN: ' + rows[0].username);
    }
    else{
      res.json({ message: "ERROR" });
    }

  });


  mysql_conn.end(); 
});

router.route('/logOut').post(function(req, res){

  var mysql_conn = db.connection();

  req.session.user = " ";
  

  console.log('Log OUT');
  res.json({ message: "You have done logOut" });


  mysql_conn.end(); 
});

router.route('/whoRU').get(function(req, res){
  res.json("El usuario es: " + req.session.user);
});



// accessed at POST http://localhost:port/addDiet
router.route('/addDiet').post(function(req, res) {
  var mysql_conn = db.connection();
  var query = "insert into Feeding(feed_id, breakfast, suplement_breakfast, time_breakfast, lunch, suplement_lunch, time_lunch, dinner, suplement_dinner, time_dinner, vitamins, feeding_notes) values('"
    +req.body.feedId+
    "', '"+req.body.breakfast+
    "', '"+req.body.suplementBreakfast+
    "', '"+req.body.timeBreakfast+
    "', '"+req.body.lunch+
    "', '"+req.body.suplementLunch+
    "', '"+req.body.timeLunch+
    "', '"+req.body.dinner+
    "', '"+req.body.suplementDinner+
    "', '"+req.body.timeDinner+
    "', '"+req.body.vitamins+"', '"+req.body.feedingNotes+"');";
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Diet Row inserted...');
  });

  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/addIronWork
router.route('/addIronWork').post(function(req, res) {
  var mysql_conn = db.connection();
  var query = "insert into Ironwork(irwork_id, lastDate_irWork, nextDate_irWork, blacksmith_name, anotations) values('"+req.body.ironWorkId+
    "', '"+req.body.lastIrWork+
    "', '"+req.body.nextIrWork+
    "', '"+req.body.blacksmithName+
    "', '"+req.body.anotations+"');";
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Ironwork Row inserted...');
  });
  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });    
});

  // accessed at POST http://localhost:port/medicalConsult
router.route('/medicalConsult').post(function(req, res) {
  var mysql_conn = db.connection();
  var query = "insert into Medical_history(mh_id, horse_id_FK, vet_id_FK, disease, medicines, anotations, date_Consult) values('"+req.body.mhId+
    "', '"+req.body.horsePatient_FK+
    "', '"+req.body.vetHorse_FK+
    "', '"+req.body.disease+
    "', '"+req.body.medicines+
    "', '"+req.body.anotations+
    "', NOW());";
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Medical consult Row inserted into medical history...');
  });
  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/registerUser
router.route('/registerUser').post(function(req, res) {
  var mysql_conn = db.connection();
  var query = "insert into User(user_id, firstName, lastName, ocupation, phone, password, age, email, username) values('"+req.body.userId+
    "', '"+req.body.firstName+
    "', '"+req.body.lastName+
    "', '"+req.body.ocupation+
    "', '"+req.body.phone+"', MD5('"+req.body.password+"'), '"+req.body.age+"', '"+req.body.email+"', '"+req.body.username+"');";
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('User Row inserted...');
  });

  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/registerHorse
router.route('/registerHorse').post(function(req, res) {
  var mysql_conn = db.connection();
  var query = "insert into Horse(horse_id, name, userid_FK, feedingid_FK, ironwork_idFK, type, breed, age) values('"+req.body.horseId+
    "', '"+req.body.horseName+
    "', '"+req.body.userHorse_FK+
    "', '"+req.body.feedingHorse_FK+
    "', '"+req.body.ironworkHorse_FK+"', '"+req.body.horseType+"', '"+req.body.horseBreed+"', '"+req.body.horseAge+"');";
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Horse Row inserted...');
  });

  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});


// accessed at POST http://localhost:port/deleteRowUsers
router.route('/deleteRowUsers').delete(function(req, res) {
  var mysql_conn = db.connection();
  var query = "delete from User where username = '"+req.body.username+"';"
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Row from users deleted...');
  });

  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});

// accessed at POST http://localhost:port/deleteRowHorses
router.route('/deleteRowHorses').delete(function(req, res) {
  var mysql_conn = db.connection();
  var query = "delete from Horse where name = '"+req.body.horseName+"';"
  
  mysql_conn.connect();
  mysql_conn.query(query, function(err, rows, fields) {
    if (err) throw err;
    console.log('Row from horses deleted...');
  });

  mysql_conn.end();
    res.json({ message: 'Statement executed: ' + query, response: "SUCCESS" });   
    
});


app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Starting server on port ' + port);